from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render, redirect

from base.models import User


def main_view(request):
    context = {"current_time": datetime.now()}
    return render(request, "base.html", context)


def register_view(request):
    if request.method == 'POST':
        if request.POST['password'] != request.POST['password2']:
            return HttpResponse("Пароли не совпадают")
        email = request.POST['email']
        password = request.POST['password']
        user = User(email=email)
        user.set_password(password)
        user.save()
        return redirect('base')

    return render(request, 'register.html')
