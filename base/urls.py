from django.urls import path

from .views import main_view, register_view

urlpatterns = [
    path('', main_view, name='base'),
    path('register/', register_view, name='register')
]

